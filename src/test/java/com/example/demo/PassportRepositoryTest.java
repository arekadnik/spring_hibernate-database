package com.example.demo;


import com.example.demo.entities.Course;
import com.example.demo.entities.Passport;
import com.example.demo.repositories.CourseRepository;
import com.example.demo.repositories.PassportRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class PassportRepositoryTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    PassportRepository passportRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    public void findByIdTest() {
        Passport passport = passportRepository.findById(10009L);
        Assert.assertEquals("1993", passport.getNumber());
    }

    @Test
    @DirtiesContext
    public void deleteById_basic() {
        passportRepository.deleteById(10002L);
        Assert.assertNull(passportRepository.findById(10002L));
    }

    @Test
    public void save_update() {
        Passport passport = passportRepository.findById(10001L);

        Assert.assertEquals("Testowo", passport.getNumber());

        passport.setNumber("Update");
        passportRepository.save(passport);

        Passport passport1 = passportRepository.findById(10001L);
        Assert.assertEquals("Update",passport1.getNumber());
    }
    @Test
    public void save_insert(){
        Passport passport = passportRepository.findById(10001L);

        Assert.assertNull(passport);

        Passport  passport1 = new Passport("test1");
        passportRepository.save(passport1);

        Passport passport2 = passportRepository.findById(10001L);
        Assert.assertEquals("test1",passport2.getNumber());

    }
    @Test
    public void getStudentByPassport(){
        Passport passport = entityManager.find(Passport.class,40001L);
        logger.info("Passport-> {}",passport);
        logger.info("Student-> {}",passport.getStudent());
    }

}
