package com.example.demo;


import com.example.demo.entities.Course;
import com.example.demo.entities.Review;
import com.example.demo.repositories.ReviewRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class ReviewRepositoryTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    ReviewRepository reviewRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    public void findByIdTest() {
        Review review = reviewRepository.findById(10012);
        Assert.assertEquals("lalalila", review.getDescription());
    }

    @Test
    @DirtiesContext
    public void deleteById_basic() {
        reviewRepository.deleteById(10012);
        Assert.assertNull(reviewRepository.findById(10012));
    }

    @Test
    public void save_update() {
        Review review = reviewRepository.findById(10012);
        Assert.assertEquals("lalalila", review.getDescription());

        review.setDescription("Update");
        reviewRepository.save(review);

        Review review1 = reviewRepository.findById(10012);
        Assert.assertEquals("Update",review1.getDescription());
    }
    @Test
    public void save_insert(){
        Review review = reviewRepository.findById(1);
        Assert.assertNull(review);

        Review  review1 = new Review("test1","dupa");
        reviewRepository.save(review1);

        Review review2 = reviewRepository.findById(1);
        Assert.assertEquals("dupa",review2.getDescription());

    }

}
