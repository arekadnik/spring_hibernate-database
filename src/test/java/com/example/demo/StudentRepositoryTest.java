package com.example.demo;


import com.example.demo.entities.Course;
import com.example.demo.entities.Student;
import com.example.demo.repositories.StudentRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class StudentRepositoryTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    StudentRepository studentRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    public void findByIdTest() {
        Student student = studentRepository.findById(10006L);
        Assert.assertEquals("Arkadiusz", student.getName());
    }

    @Test
    @DirtiesContext
    public void deleteById_basic() {
        studentRepository.deleteById(10006L);
        Assert.assertNull(studentRepository.findById(10006L));
    }

    @Test
    public void save_update() {
        Student student = studentRepository.findById(10006L);
        Assert.assertEquals("Arkadiusz", student.getName());

        student.setName("Update");
        studentRepository.save(student);

        Student student1 = studentRepository.findById(10006L);
        Assert.assertEquals("Update",student1.getName());
    }
    @Test
    public void save_insert(){
        Student student = studentRepository.findById(10006L);
        Assert.assertNull(student);

        Student  student1 = new Student("Arkadiusz");
        studentRepository.save(student1);

        Student student2 = studentRepository.findById(1);
        Assert.assertEquals("Arkadiusz",student2.getName());

    }

    @Test
    public void getStudentWithPassportTest(){
        Student student = studentRepository.findById(20001L);
        logger.info("Student -> {}",student);
        logger.info("Student -> {}",student.getPassport());
    }
    @Test
    public void getStudentsWithCourses(){
        Student student = entityManager.find(Student.class,20001L);
        logger.info("Student -> {}",student);
        logger.info("Kursy -> {}",student.getCourseList());
    }
}
