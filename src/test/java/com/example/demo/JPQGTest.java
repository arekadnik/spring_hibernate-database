package com.example.demo;


import com.example.demo.entities.Course;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class JPQGTest {
    @Autowired
    EntityManager em;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Test
    public void getAllCoursesTest(){
        List resultList = em.createQuery("Select c from Course c").getResultList();
        logger.info("Kurs -> {}",resultList);
    }

    @Test
    public void getAllTestInWorld() {

        List courses = em.createQuery("SELECT c FROM Course c WHERE c.name LIKE '%3%'").getResultList();
        logger.info("Courses included 3 in the name -> {}", courses);
    }
    @Test
    public void getRcord() {
        List courses = em.createQuery("SELECT c FROM Course c WHERE c.name LIKE '%Test%'").getResultList();
        logger.info("Courses included 3 in the name -> {}", courses);
    }

    @Test
    public void getUpperName() {

        //Type Querry
//        TypedQuery<Course> typedQuery = em.createQuery("SELECT upper (c.name) FROM Course c",Course.class);
        List courses = em.createQuery("SELECT upper (c.name) FROM Course c").getResultList();
        logger.info("Courses included 3 in the name -> {}", courses);
    }

    @Test
    public void getListBetween() {
        List courses = em.createQuery("SELECT c FROM Course c WHERE c.id between 10003 and 10005").getResultList();
        logger.info("Courses included 3 in the name -> {}", courses);
    }

    @Test
    public void getAllCourseseByName(){
        List coursese =em.createNamedQuery("select all").getResultList();
        logger.info("Kursy -> {}",coursese);
    }
}
