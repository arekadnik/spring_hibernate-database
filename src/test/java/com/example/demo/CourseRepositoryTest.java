package com.example.demo;


import com.example.demo.entities.Course;
import com.example.demo.entities.Review;
import com.example.demo.repositories.CourseRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class CourseRepositoryTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    CourseRepository coureRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    public void findByIdTest() {
        Course course = coureRepository.findById(10001L);
        Assert.assertEquals("Testowo", course.getName());
    }

    @Test
    @DirtiesContext
    public void deleteById_basic() {
        coureRepository.deleteById(10002L);
        Assert.assertNull(coureRepository.findById(10002L));
    }

    @Test
    public void save_update() {
        Course course = coureRepository.findById((10001L));
        Assert.assertEquals("Testowo", course.getName());

        course.setName("Update");
        coureRepository.save(course);

        Course course1 = coureRepository.findById(10001L);
        Assert.assertEquals("Update", course1.getName());
    }

    @Test
    public void save_insert() {
        Course course = coureRepository.findById(1);
        Assert.assertNull(course);

        Course course1 = new Course("test1");
        coureRepository.save(course1);

        Course course2 = coureRepository.findById(1);
        Assert.assertEquals("test1", course2.getName());

    }

    @Test
    @DirtiesContext
    public void playWithEmTest() {
        coureRepository.playWithEm();
    }

    @Test
    @Transactional
    public void getCourseWithReviews() {
        Course course = entityManager.find(Course.class,10001L);
        logger.info("Kurs -> {}",course);
        logger.info("Oceny -> {}",course.getRv());
    }
    @Test
    @Transactional
    public void getCourseViaReview(){
        Review review = entityManager.find(Review.class,30001L);
        logger.info("Ocena -> {}",review);
        logger.info("Kurs -> {}",review.getCourse());
    }


}
