--insert into Course(id, name) values(10001, 'Testowo');
--insert into Course(id, name) values(10002, 'Testowo2');
--insert into Course(id, name) values(10003, 'Testowo3');
--insert into Course(id, name) values(10004, 'Testowo4');
--insert into Course(id, name) values(10005, 'Testowo5');
--insert into Student(id, name) values(10006, 'Arkadiusz');
--insert into Student(id, name) values(10007, 'Bartosz');
--insert into Student(id, name) values(10008, 'Jakub');
--insert into Passport(id, number) values(10009, '1993');
--insert into Passport(id, number) values(10010, '1995');
--insert into Passport(id, number) values(10011, '1996');
--insert into Review(id, rating, description) values(10012, 'best','lalalila');
--insert into Review(id, rating, description) values(10013, 'the best','dadada');
--insert into Review(id, rating, description) values(10014, 'looser','blablabla');


insert into Course(id, name) values(10001, 'Testowo');
insert into Course(id, name) values(10002, 'Testowo2');
insert into Course(id, name) values(10003, 'Testowo3');
insert into Course(id, name) values(10004, 'Testowo4');
insert into Course(id, name) values(10005, 'Testowo5');

insert into passport(id, number) values(40001, 'A1234');
insert into passport(id, number) values(40002, 'B4567');
insert into passport(id, number) values(40003, 'C7890');
insert into student(id, name, passport_id) values(20001, 'Jan', 40001);
insert into student(id, name, passport_id) values(20002, 'Beata', 40002);
insert into student(id, name, passport_id) values(20003, 'Zofia', 40003);


insert into Review(id, rating, description, course_id) values(30001, '5','Super',10001);
insert into Review(id, rating, description, course_id) values(30002, '4','git',10001);
insert into Review(id, rating, description, course_id) values(30003, '1','gniot',10001);
insert into Review(id, rating, description, course_id) values(30004, '2','sprwadzCzyOk',10001);

Insert into student_course(student_id, course_id) VALUES (20001, 10001);
Insert into student_course(student_id, course_id) VALUES (20001, 10002);
Insert into student_course(student_id, course_id) VALUES (20002, 10001);
Insert into student_course(student_id, course_id) VALUES (20001, 10003);
Insert into student_course(student_id, course_id) VALUES (20002, 10002);
Insert into student_course(student_id, course_id) VALUES (20003, 10002);
