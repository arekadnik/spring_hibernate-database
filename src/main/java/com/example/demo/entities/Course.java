package com.example.demo.entities;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "course")
@NamedQueries(value = {
        @NamedQuery(name = "select_all", query = "select c from Course c"),
        @NamedQuery(name = "select_all_with_test",
                query = "select c from Course c where c.name like 'Test%'")
})
public class Course {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;
    @ManyToMany(mappedBy = "courseList")
    private List<Student> studentList = new ArrayList<>();

    public List<Student> getStudentList() {
        return studentList;
    }

    @OneToMany(mappedBy = "course", fetch = FetchType.EAGER)
    private List<Review> rv = new ArrayList<>();


    public Course(String name) {
        this.name = name;
    }

    protected Course() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public List<Review> getRv() {
        return rv;
    }

    public void addRv(Review rv) {
        this.rv.add(rv);
    }

    public void addStudent(Student student) {
        studentList.add(student);
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

