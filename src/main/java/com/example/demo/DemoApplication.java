package com.example.demo;

import com.example.demo.entities.Course;
import com.example.demo.entities.Review;
import com.example.demo.entities.Student;
import com.example.demo.extendsDemo.Employee;
import com.example.demo.extendsDemo.EmployeeRepository;
import com.example.demo.extendsDemo.FullTimeJob;
import com.example.demo.extendsDemo.PartTimeJob;
import com.example.demo.repositories.CourseRepository;
import com.example.demo.repositories.ReviewRepository;
import com.example.demo.repositories.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.ArrayList;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
    @Autowired
    CourseRepository coureRepository;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    EmployeeRepository employeeRepository;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        FullTimeJob arek = new FullTimeJob("Arek",new BigDecimal(4000));
        PartTimeJob kuba = new PartTimeJob("Kuba", new BigDecimal(10));
        employeeRepository.save(arek);
        employeeRepository.save(kuba);

//
//        Student student = new Student("LLL");
//        Course course = new Course("ZZZZ");
//        studentRepository.saveStudentWithCourse(student,course);
//        studentRepository.saveManyStudentsWithManyCourses();
//        ArrayList<Review> list = new ArrayList<>();
//        list.add(new Review("5","Super"));
//        list.add(new Review("1","Gniot"));
//        studentRepository.saveStudentWithPassport();
//        Course course = coureRepository.findById(10001L);
//        logger.info("Course 10001 -> {}",course);

    }

}
