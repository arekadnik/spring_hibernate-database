package com.example.demo.repositories;

import com.example.demo.entities.Passport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class PassportRepository {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    EntityManager em;

    public Passport findById(long id) {
        return em.find(Passport.class, id);
    }

    public void deleteById(long id) {
        Passport passportToDelete = findById(id);
        em.remove(passportToDelete);
    }

    public Passport save(Passport passport ) {
        if (passport.getId() == null) {
            em.persist(passport);
        } else {
            em.merge(passport);
        }
        return passport;
    }

}
