package com.example.demo.repositories;

import com.example.demo.entities.Course;
import com.example.demo.entities.Passport;
import com.example.demo.entities.Review;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.validation.constraints.Max;
import java.util.List;

@Repository
@Transactional
public class ReviewRepository {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    EntityManager em;

    public Review findById(long id) {
        return em.find(Review.class, id);
    }

    public void deleteById(long id) {
        Review reviewToDelete = findById(id);
        em.remove(reviewToDelete);
    }

    public Review save(Review review) {
        if (review.getId() == null) {
            em.persist(review);
        } else {
            em.merge(review);
        }
        return review;
    }

    public void addReviewWithCourse(Long courseId, List<Review> reviewList) {
        Course course = em.find(Course.class, courseId);
        for (Review rv : reviewList) {
            course.addRv(rv);
            rv.setCourse(course);
            em.persist(rv);
        }

    }

}
