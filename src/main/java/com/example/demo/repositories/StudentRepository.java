package com.example.demo.repositories;

import com.example.demo.entities.Course;
import com.example.demo.entities.Passport;
import com.example.demo.entities.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;


@Repository
@Transactional
public class StudentRepository {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    EntityManager em;

    public Student findById(long id) {
        return em.find(Student.class, id);
    }

    public void deleteById(long id) {
        Student studentToDelete = findById(id);
        em.remove(studentToDelete);
    }

    public Student save(Student student) {
        if (student.getId() == null) {
            em.persist(student);
        } else {
            em.merge(student);
        }
        return student;
    }

    public void saveStudentWithPassport() {
        Passport passport = new Passport("AADDDBBB");
        em.persist(passport);
        Student student = new Student("LOLEK");
        student.setPassport(passport);
        em.persist(student);

    }

    public void saveManyStudentsWithManyCourses() {
        Student student = new Student("Bolek");
        Student student1 = new Student("Lolek");

        em.persist(student);
        em.persist(student1);

        Course course = new Course("ZZZZ");
        em.persist(course);

        course.addStudent(student);
        course.addStudent(student1);

        student.addCourse(course);
        student1.addCourse(course);

        em.merge(student);
        em.merge(student1);


    }
    public void saveStudentWithCourse(Student student, Course course){

em.persist(student);
em.persist(course);
student.addCourse(course);
course.addStudent(student);
em.merge(student);
em.merge(course);

    }

}
