package com.example.demo.repositories;


import com.example.demo.entities.Course;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Transactional
@Repository
public class CourseRepository {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    EntityManager em;

    public Course findById(long id) {
        return em.find(Course.class, id);
    }

    public void deleteById(long id) {
        Course coruseToDelete = findById(id);
        em.remove(coruseToDelete);
    }

    public Course save(Course course) {
        if (course.getId() == null) {
            em.persist(course);
        } else {
            em.merge(course);
        }
        return course;
    }

    public void playWithEm() {

        logger.info("Jestem jestem w metodzie playWithmME ");

        Course course = new Course("AAAAA");
        em.persist(course);
        em.flush();
        em.detach(course);
        course.setName("BBBBB");

        logger.info("Jestem jestem w metodzie playWithmME ");
    }
}
