package com.example.demo.extendsDemo;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
public class FullTimeJob extends Employee {

    private BigDecimal salary;

    public FullTimeJob(String name, BigDecimal salary) {
        super(name);
        this.salary = salary;
    }

    protected FullTimeJob() {
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }
}
