package com.example.demo.extendsDemo;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
public class PartTimeJob extends Employee{

    private BigDecimal hourlyWage;

    public PartTimeJob(String name, BigDecimal hourlyWage) {
        super(name);
        this.hourlyWage = hourlyWage;
    }

    protected PartTimeJob() {
    }

    public BigDecimal getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(BigDecimal hourlyWage) {
        this.hourlyWage = hourlyWage;
    }
}
